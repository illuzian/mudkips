from django.db import models
from django.contrib.auth.models import User

class Category(models.Model):
    cat_name = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = "Categories"
    def __unicode__(self):
        return self.cat_name

class Post(models.Model):
    post_title = models.CharField(max_length=100)
    post_body = models.TextField()
    post_category = models.ManyToManyField(Category)
    post_date = models.DateTimeField(auto_now=True)
    post_author = models.ForeignKey(User)
    post_published = models.BooleanField()
    
    def is_published(self):
        return self.post_published
    
    def __unicode__(self):
        return self.post_title
        
#class Comment(models.Model):
#    comment_title = models.CharField(max_length=100)
#    comment_body = models.TextField()
#    comment_date = models.DateTimeField(auto_now=True)
#    comment_author = models.ForeignKey(User)
#    comment_moderating = models.BooleanField()
#    comment_post = models.ForeignKey(Post)
#    def __unicode__(self):
#        return self.comment_title
    
class Settings(models.Model):
    setting_commentmod = models.BooleanField()
    
class Nav(models.Model):
    nav_title = models.CharField(max_length=50)
    nav_url = models.CharField(max_length=200)
    nav_dropdown = models.ForeignKey('self', blank=True, null=True)
    def __unicode__(self):
        return self.nav_title
    
    

    
    
    