from django.http import HttpResponse
from django.template import RequestContext, loader
from django.shortcuts import render, get_object_or_404, render_to_response
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from blog.models import Post, Nav

def index(request):
    #template = loader.get_template('blog/index.html')
    posts = Post.objects.all()
    paginator = Paginator(posts, 4)
    page = request.GET.get('page')
    try:
        pages = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        pages = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        pages = paginator.page(paginator.num_pages)



    context = RequestContext(request, {
            'paginator': paginator,
            'posts' : posts,
            'pages' : pages,
            'btitle' : "Luz's Blog",
            'blink' : "/blog",
            'Nav' : Nav.objects.all(),
    })
    return render_to_response('blog/index.html', context )
    
def detail(request, post_id):
    template = loader.get_template('blog/post.html')
    try:
        post = Post.objects.get(pk=post_id)
        
    except Post.DoesNotExist:
        raise Http404

#    comments = post.comment_set.all()
    context = RequestContext(request, {
            'post': post,
            'btitle' : "Luz's Blog",
            'blink' : "/blog",
            'Nav' : Nav.objects.all(),
    })
    return HttpResponse(template.render(context))
    