from django.conf.urls import patterns, url

from blog import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^read/(?P<post_id>\d+)/$', views.detail, name='detail'),
#    url(r'^comments/', include('django.contrib.comments.urls')),
)