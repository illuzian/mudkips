from django.contrib import admin

from blog import models as o

class PostAdmin(admin.ModelAdmin):
    post_date = 'post_date'
    def save_model(self, request, obj, form, change): 
        obj.post_author = request.user
        obj.save()
        
#class CommentAdmin(admin.ModelAdmin):
#    post_date = 'comment_date'
#    def save_model(self, request, obj, form, change): 
#        obj.comment_author = request.user
#        obj.save()

admin.site.register(o.Category)
admin.site.register(o.Nav)
admin.site.register(o.Post, PostAdmin)
#admin.site.register(o.Comment, CommentAdmin)


